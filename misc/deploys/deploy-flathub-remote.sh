#!/usr/bin/env sh

flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo &&

exit 0
