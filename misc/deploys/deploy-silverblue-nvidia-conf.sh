#!/usr/bin/env bash

echo "Downloading and installing NVIDIA Drivers..."
rpm-ostree install akmod-nvidia xorg-x11-drv-nvidia-cuda xorg-x11-drv-nvidia-power &&

echo ""
echo "Updating Kernel args..."
rpm-ostree kargs --append=rd.driver.blacklist=nouveau --append=modprobe.blacklist=nouveau --append=nvidia-drm.modeset=1 &&

echo ""
echo "Done!"
exit 0
