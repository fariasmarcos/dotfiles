# Dotfiles
Files to speed up **my** Fedora Linux setup. Some of these can be used to setup other distributions.

## How to use

### Fedora Linux
Just clone the repository and copy recursively all the files inside the `homedir/` folder to your `$HOME` directory, or you can just run the `install-homedir.sh` script.

### Other distributions
After taking the steps above, past the code below at the end of your `~/.bashrc` file:

```bash
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc
```

### Fonts, Apps, Fedora's Toolbox tools and Miscellaneous
There's a bunch of stuff available to install/configure inside each folder besides `homedir/`. Most of them are just shell scripts, one-click-to-install kinda thing.

## Highlights
`apps/jellyfin-media-server` - Install Jellyfin Media Server inside a `podman` container, with automatic access to the `xdg-videos` directory.

`toolbox/` - Templates to setup some aliases to make easier to manage the default toolbox from the CLI. They can be installed in the same way as the files inside the `homedir/` folder, just by copying the files to your `$HOME` directory. An executable script called `install-toolbox-env.sh` is also available.

`toolbox/usr/local/bin/xdg-open` - A script to enable the host's XDG functionality from inside the toolbox environment. Once running the toolbox, copy it inside `/usr/local/bin/` and enjoy!