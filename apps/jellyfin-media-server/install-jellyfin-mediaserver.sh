#!/usr/bin/env bash

if [ $# -lt 1 ] ; then
    MEDIA_FOLDER="$(xdg-user-dir VIDEOS)"
else
    MEDIA_FOLDER="$1"
fi

JELLYFIN_CACHE="$HOME/.local/srv/jellyfin/cache"
JELLYFIN_CONFIG="$HOME/.local/srv/jellyfin/config"
JELLYFIN_SERVER_NAME="jellyfin-media-server"

echo "Media folder defined as ${MEDIA_FOLDER:-"none"}."
echo "If the folder is defined as \"none\", check if xdg-user-dir tool is installed."
read -p "Is that OK? (ENTER/Ctrl+C)"

# create folders
echo "Creating Jellyfin dirs..."
mkdir -p "$JELLYFIN_CACHE" "$JELLYFIN_CONFIG" &&

# downloading image
echo "Downloading server..."
podman pull docker.io/jellyfin/jellyfin &&

# create container
echo "Installing server..."
podman run \
    --cgroup-manager=systemd \
    --volume $JELLYFIN_CONFIG:/config:z \
    --volume $JELLYFIN_CACHE:/cache:z \
    --volume $MEDIA_FOLDER:/media:z \
    -p 8096:8096 \
    --label "io.containers.autoupdate=image" \
    --name $JELLYFIN_SERVER_NAME \
    docker.io/jellyfin/jellyfin \
    &
# wait until auto-setup finishes
sleep 30 &&
echo "Done!"

# install systemd service
echo "Installing service..."
mkdir -p $HOME/.config/systemd/user &&
podman generate systemd --new --name $JELLYFIN_SERVER_NAME > $HOME/.config/systemd/user/$JELLYFIN_SERVER_NAME.service &&
systemctl --user enable $JELLYFIN_SERVER_NAME.service &&
echo "Done!"
