#!/usr/bin/env bash

# truncate default run and enter commands
alias tbx="toolbox run"
alias tbe="toolbox enter"

# maintenance commands
alias tbx-upgrade="toolbox run sudo dnf upgrade -y"
alias tbx-cleanup="toolbox run sudo dnf autoremove -y"

