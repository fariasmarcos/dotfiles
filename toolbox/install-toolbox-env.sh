#!/usr/bin/env bash

if [ -z ${TOOLBOX_PATH+x} ]; then
    echo "Error: not running inside a toolbox container."
    exit 1
fi

# install custom utils and repos for toolbox
sudo cp -R -vf usr/ etc/ / &&

# install aliases for bash
cp -R -vf .bashrc.d/ ~/ &&

echo "Done!" &&
exit 0
