#!/usr/bin/env bash

#flatpak
alias flsapp="flatpak list --app --columns=application,version,branch,installation"
alias flsruntime="flatpak list --runtime --columns=application,version,branch,origin,installation"
alias flsearch="flatpak search --columns=name,application,version,branch,remotes"

#ls
alias la="ls -a"
alias ll.="ls -lha"

#mkdir
alias mkdir="mkdir -p"
