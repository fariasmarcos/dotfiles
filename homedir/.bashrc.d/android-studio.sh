#!/usr/bin/env bash

# add Studio's bin_dir to PATH if it exists
if [ -d /opt/android-studio/bin ]; then
	PATH="$PATH:/opt/android-studio/bin"

	# alias to start bin_dir/studio.sh
	alias studio="studio.sh"
fi

# add plataform-tools dir to PATH if it exists
if [ -d ~/Android/Sdk/platform-tools ]; then
	PATH="$PATH:~/Android/Sdk/platform-tools"
fi

export PATH
