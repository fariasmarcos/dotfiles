#!/usr/bin/env bash
cp -R -fv .bashrc.d/ .local/ ~/ &&
cp -nv .gitconfig.template ~/.gitconfig &&
exit 0
