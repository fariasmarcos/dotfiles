#!/usr/bin/env sh

echo "Creating folders..." &&
mkdir -p ./tempfiles/ &&
cd ./tempfiles/ &&

echo "Downloading Hack Fonts..." &&
curl -L -s https://api.github.com/repos/source-foundry/Hack/releases/latest -o info &&
curl -LC - $(awk -F '"' "/browser_download_url/ {print \$4}" info | grep ttf.zip) -o hack-font-comp &&

echo "Extracting files..." &&
mkdir -p ./.local/share/fonts/Hack/ &&
unzip -o hack-font-comp >/dev/null 2>&1 &&
cp -R -f ./ttf/*.ttf ./.local/share/fonts/Hack/ &&

echo "Installing..." &&
cp -R -vf ./.local/ $HOME/ &&
fc-cache -f &&

echo "Cleaning up..." &&
cd ../ &&
rm -rf ./tempfiles/ &&
echo "Done!"

exit 0