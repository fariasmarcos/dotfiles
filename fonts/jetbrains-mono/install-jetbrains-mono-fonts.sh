#!/usr/bin/env sh

echo "Creating folders..." &&
mkdir -p ./tempfiles/ &&
cd ./tempfiles/ &&

echo "Downloading JetBrains Mono fonts..." &&
curl -L -s https://api.github.com/repos/JetBrains/JetBrainsMono/releases/latest -o info &&
curl -LC - $(awk -F '"' "/browser_download_url/ {print \$4}" info) -o jetbrains-mono-comp &&

echo "Extracting files..." &&
mkdir -p ./.local/share/fonts/JetBrainsMono/ &&
unzip -o jetbrains-mono-comp >/dev/null 2>&1 &&
cp -fR ./fonts/ttf/* ./.local/share/fonts/JetBrainsMono/ &&
rm -rf ./fonts/ &&

echo "Installing..." &&
cp -R -vf ./.local/ $HOME/ &&
fc-cache -f &&

echo "Cleaning up..." &&
cd ../ &&
rm -rf ./tempfiles/ &&
echo "Done!"

exit 0