#!/usr/bin/env sh

echo "Creating folders..." &&
mkdir -p ./tempfiles/ &&
cd ./tempfiles/ &&

echo "Downloading Inter fonts..." &&
curl -L -s https://api.github.com/repos/rsms/inter/releases/latest -o info &&
curl -LC - $(awk -F '"' "/browser_download_url/ {print \$4}" info) -o inter-font-comp &&

echo "Extracting files..." &&
mkdir -p ./.local/share/fonts/Inter/ &&
unzip -o inter-font-comp >/dev/null 2>&1 &&
cp -fR ./Inter\ Desktop/*.otf ./.local/share/fonts/Inter/ &&

echo "Installing..." &&
cp -R -vf ./.local/ $HOME/ &&
fc-cache -f &&

echo "Cleaning up..." &&
cd ../ &&
rm -rf ./tempfiles/ &&
echo "Done!"

exit 0